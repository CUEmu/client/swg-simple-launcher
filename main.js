const { app, BrowserWindow, Menu, shell, ipcMain, dialog } = require("electron");
const store = require("electron-store");
const os = require("os");
const fs = require("fs");
const packageInfo = require("./package.json");
const log = require("electron-log");
const net = require("net");
const { parseString } = require("xml2js");
const path = require("path");
const replace = require("replace-in-file");
const childProcess = require("child_process");
const crypto = require("crypto");
const https = require("https");

const userSettings = new store();

let mainWindow, aboutWindow, newServerWindow, manageClientsWindow;

let serverKeys = (!userSettings.has("servers")) ? [] : Object.keys(userSettings.get("servers"));
let clientKeys = (!userSettings.has("clients")) ? [] : Object.keys(userSettings.get("clients"));

class ProgressBar {
    inc = 0;
    percentage = 0;

    constructor(amount) {
        this.percentage = amount;
    }

    add(total) {
        this.inc = (total !== 0 ? this.inc || 0 : -1) + 1;
        this.percentage = Math.floor(this.inc / total * 100);
    }

    get() {
        return this.percentage;
    }

    reset() {
        this.percentage = 0;
        this.inc = 0;
    }
}

const Progress = new ProgressBar(0);

const versionToEraList = {
    "0.0.119.798": "Pre-CU",
    "0.0.140.646": "CU",
    "0.0.203.189": "NGE",
    "1.0.0.1": "Legends"
}

let contentManifest = {};

function createWindow() {
	mainWindow = new BrowserWindow({
		width: 300,
		height: 420,
		webPreferences: {
            nodeIntegration: false,
            preload: path.join(__dirname, "./preload/indexPreload.js")
        },
        icon: path.join(__dirname, "/icon.ico")
	});

	mainWindow.loadFile("./html/index.html");

	mainWindow.on("closed", () => {
		mainWindow = null
    });

	const menuTemplate = [
		{
			label: "Menu",
			submenu: [
				{
					label: "Server Browser",
					click: () => {
						mainWindow.loadFile("./html/index.html")
					}
				},
				{
					label: "Profession Calculator",
					click: () => {
                        if ((!userSettings.has("servers") || serverKeys.length == 0) || (!userSettings.has("clients") || clientKeys.length == 0)) {
                            dialog.showMessageBox(mainWindow, {
                                type: "error",
                                title: "No Clients or Servers Found",
                                message: "You must add a client and server before adding a profession calculator",
                                buttons: ["Ok"]
                            });

                            return;
                        }

                        mainWindow.webContents.executeJavaScript("document.getElementById('serverList')[document.getElementById('serverList').selectedIndex].value", false).then((serverId) => {
                            if (!userSettings.get("clients." + clientKeys[clientKeys.findIndex(item => item === userSettings.get("servers." + serverId + ".client").toString())] + ".calculator")) {
                                const fileOptions = {
                                    title: "Locate Profession Calculator for " + userSettings.get("clients." + clientKeys[clientKeys.findIndex(item => item === userSettings.get("servers." + serverId + ".client").toString())] + ".name") ,
                                    filters: [
                                        { name: "Executable", extensions: ["exe"] }
                                    ],
                                    defaultPath: userSettings.get("clients." + clientKeys[clientKeys.findIndex(item => item === userSettings.get("servers." + serverId + ".client").toString())] + ".path")
                                }
                                
                                const filePath = dialog.showOpenDialog(mainWindow, fileOptions);
                    
                                try {
                                    userSettings.set("clients." + clientKeys[clientKeys.findIndex(item => item === userSettings.get("servers." + serverId + ".client").toString())] + ".calculator", filePath[0]);
                                } catch(e) {
                                    log.error("No file selected for profession calculator executable check");
                                    //log.error(e);
                                }
                            }

                            const calculatorPath = userSettings.get("clients." + clientKeys[clientKeys.findIndex(item => item === userSettings.get("servers." + serverId + ".client").toString())] + ".calculator");

                            childProcess.spawn(calculatorPath, [], { cwd: path.dirname(calculatorPath), detached: true, stdio: "ignore" }).unref();
                        }).catch((e) => {
                            log.error("Cannot find selected index of server list for profession calculator");
                            //log.error(e);
                        });
					}
				},
				{
					type: "separator"
				},
				{
					role: "close"
				}
			]
		},
		{
			label: "View",
			submenu: [
				{
					role: "reload"
				},
				{
					role: "forcereload"
				}
			]
		},
		{
			role: "help",
			submenu: [
				{
					label: "Open Logs",
					click: () => {
						shell.openItem(os.homedir() + `\\AppData\\Roaming\\${packageInfo.name}\\log.log`);
					}
				},
				{
					label: "Open Config",
					click: () => {
						shell.openItem(os.homedir() + `\\AppData\\Roaming\\${packageInfo.name}\\config.json`);
					}
				},
				{
					label: "About",
					click: () => {
						aboutWindow = new BrowserWindow({
							width: 200,
							height: 300,
							title: "About",
							parent: mainWindow,
							modal: true,
							show: false,
							minimizable: false,
							maximizable: false,
							autoHideMenuBar: true,
							webPreferences: {
								nodeIntegration: false
							}
						});

						aboutWindow.loadFile('./html/about.html')

						aboutWindow.once("ready-to-show", () => {
							aboutWindow.show()
						});

						aboutWindow.webContents.on("will-navigate", (event, address) => {
							event.preventDefault()
							shell.openExternal(address)
						});

						aboutWindow.on("closed", () => {
							aboutWindow = null
						});
					}
                },
                /*{
					label: "Live CFG Test",
					click: () => {
                        mainWindow.webContents.executeJavaScript("document.getElementById('serverList').selectedIndex", false).then((index) => {
                            editLiveConfig(index)
                        }).catch((e) => {
                            log.error("Cannot find selected index of server list for Live CFG Test");
                            //log.error(e);
                        });
					}
				},*/
				/*{
					type: "separator"
                },
				{
					role: "toggledevtools"
				},*/
			]
		}
	];

	const appMenu = Menu.buildFromTemplate(menuTemplate);
	Menu.setApplicationMenu(appMenu);
}

function showVerificationResponseDialog(corruptFiles, missingFiles) {
	if (corruptFiles.length == 0 && missingFiles.length == 0) {
		dialog.showMessageBox(mainWindow, {
			type: "info",
			title: "Installation Integrity Results",
			message: "Verification of installation integrity has succeeded.",
			buttons: ["Ok"]
		});

		return;
	}

	let corruptedFilesString = "";

	for (let i = 0; i < corruptFiles.length; i++) {
		corruptedFilesString += corruptFiles[i] + "\n";
	}

	let missingFilesString = "";

	for (let i = 0; i < missingFiles.length; i++) {
		missingFilesString += missingFiles[i] + "\n";
	}

	const combinedString = ((corruptFiles.length > 0) ? "The following files are corrupt:\n" + corruptedFilesString + "\n" : "") + ((missingFiles.length > 0) ? "The following files are missing:\n" + missingFilesString : "");

	dialog.showMessageBox(mainWindow, {
		type: "error",
		title: "Installation Integrity Results",
		message: "Verification of installation integrity has failed.",
		detail: combinedString,
		buttons: ["Ok"]
	});
}

function editLoginConfig(index) {
    const addrRegexp = new RegExp("loginServerAddress0=(.*)", "i");
	const portRegexp = new RegExp("loginServerPort0=(.*)", "i");

	const newAddress = "loginServerAddress0=" + userSettings.get("servers." + index + ".address");
	const newPort = "loginServerPort0=" + userSettings.get("servers." + index + ".port")

	const options = {
		files: userSettings.get("clients." + clientKeys[clientKeys.findIndex(item => item === userSettings.get("servers." + index + ".client").toString())] + ".loginFile"),
		from: [addrRegexp, portRegexp],
		to: [newAddress, newPort]
	}

    const results = replace.sync(options);

    if (results[0].hasChanged == true)
        log.info("Login configuration has been successfully changed");
}

function downloadFile(downloadUrl, destination, totalDownloadSize, showDialog) {
    const request = https.get(downloadUrl, (response) => {
        if (response.statusCode > 300 && response.statusCode < 400 && response.headers.location) {
            return downloadFile(response.headers.location, destination);
        } else if (response.statusCode !== 200) {
            log.error("Response status was " + response.statusCode + " for " + downloadUrl);
            return;
        }

        const fileWriter = fs.createWriteStream(destination, {flags: "a"});

        response.pipe(fileWriter);

        response.on("data", (chunk) => {
            mainWindow.webContents.send("updateVerificationProgress", chunk.length / totalDownloadSize * 100);
        });

        response.on("end", () => {
            mainWindow.webContents.executeJavaScript("document.getElementById('launch').removeAttribute('disabled');");
            mainWindow.webContents.executeJavaScript(`document.getElementById("verifyProgressBackground").hidden = true;`);

            Progress.reset();

            if (showDialog) {
                dialog.showMessageBox(mainWindow, {
                    type: "info",
                    title: "Download Successful",
                    message: "Extra content has been downloaded successfully.",
                    buttons: ["Ok"]
                });
            }
        });

        fileWriter.on("error", (error) => {
            fileWriter.end();
            log.error(error);
        });

        fileWriter.on("finish", () => {
            fileWriter.end();
        });
    });

    request.on("error", (error) => {
        log.error(error);
    });
}

function editLiveConfig(index) {
    try {
        const executablePath = userSettings.get("clients." + clientKeys[clientKeys.findIndex(item => item === userSettings.get("servers." + index + ".client").toString())] + ".executable");
        const stdout = childProcess.execSync('wmic datafile where name="'+ executablePath.split("\\").join("\\\\") + '" get Version');

        const executableVersion = stdout.toString().split("\n")[1].trim();
        const clientEra = versionToEraList[executableVersion].toLowerCase();

        let cfgPath = path.dirname(executablePath);

        if (clientEra == "pre-cu")
            cfgPath += "\\swgemu_live.cfg";
        else
            cfgPath += "\\live.cfg"

        let data = fs.readFileSync(cfgPath);

        if (data.includes("[SharedFile]")) {
            const maxSearchPriority = data.toString().match(/maxSearchPriority=(.*)/i);
            const currentPriority = maxSearchPriority[1];
            let additionalPriority = Object.keys(contentManifest).length;

            let replacementString = "";

            // Build the new string that contains all of the additional tre files; need to decrement and reverse the check from a typical one in order to go in _descending_ order in the live.cfg
            for (let i = (additionalPriority - 1); i >= 0; i--) {
                if (data.includes(contentManifest[i].name)) {
                    additionalPriority--;
                    continue;
                }
                
                let startingPoint = 0;

                if (data.includes("searchTree_")) {
                    startingPoint = parseInt(data.toString().match(/searchTree_\d+_(\d+)=/i)[1]);
                }

                startingPoint += i;

                replacementString += "\r\n\tsearchTree_00_" + startingPoint + "=" + contentManifest[i].name;
            }

            if (additionalPriority != 0) {
                const newPriority = parseInt(currentPriority) + additionalPriority;

                replacementString = "maxSearchPriority=" + newPriority + replacementString;

                // Replace it after it's built to add all of the new tres
                const options = {
                    files: cfgPath,
                    from: maxSearchPriority[0],
                    to: replacementString
                }
            
                const results = replace.sync(options);
                
                if (results[0].hasChanged == true)
                    log.info("Live configuration has been successfully changed");
            }
        } else {
            log.error("Live configuration file is not properly formatted; there is no SharedFile section.");
        }
    } catch(e) {
        log.error(e);
    }
}

app.on("ready", () => {
	createWindow();
});

app.on("window-all-closed", () => {
	if (process.platform !== "darwin") 
		app.quit();
});

app.on("activate", () => {
	if (mainWindow === null)
        createWindow();
});

ipcMain.on("getServerStatus", (event, index) => {
	const address = userSettings.get("servers." + index + ".address");

	const serverStatus = net.createConnection(44455, address, () => {
		serverStatus.on("data", (data) => {
			parseString(data, (error, result) => {
                mainWindow.webContents.send("serverStatusResponse", result["zoneServer"]["status"][0].toString());
                mainWindow.webContents.executeJavaScript("document.getElementById('launch').removeAttribute('disabled');");
			});
		});
	});

	serverStatus.on("error", (error) => {
        if (error.toString().includes("Error: connect ECONNREFUSED")) { // Log that the server status isn't being reached
            mainWindow.webContents.send("serverStatusResponse", "unknown");

            log.info("Server status cannot be reached...");
        } else if (!error.toString().includes("Error: read ECONNRESET")) { // Log that literally any other actual error occured other than ECONNRESET as that's just a nusiance error
            mainWindow.webContents.send("serverStatusResponse", "unknown");

            log.error(error);
        }
		
        serverStatus.end();
        
        mainWindow.webContents.executeJavaScript("document.getElementById('launch').removeAttribute('disabled');");
    });
});

ipcMain.on("removeServer", (event, index) => {
    try {
        dialog.showMessageBox(mainWindow, {
            type: "warning",
            title: "Remove Server",
            message: "Are you sure you want to remove this server?",
            buttons: ["No", "Yes"]
        }, (response) => {
            if (response == 1) {
                userSettings.delete("servers." + index);
                mainWindow.reload();
            }
        });
    } catch(e) {
        log.error("No servers exist");
        //log.error(e);
    }
});

ipcMain.on("createNewServerWindow", () => {
    if (!userSettings.has("clients") || clientKeys.length == 0) {
        dialog.showMessageBox(mainWindow, {
            type: "error",
            title: "No Clients Found",
            message: "You must add a client before adding a server",
            buttons: ["Ok"]
        });

        return;
    }

	newServerWindow = new BrowserWindow({
		width: 600,
		height: 145,
		title: "Add Server",
		parent: mainWindow,
		modal: true,
		show: false,
		minimizable: false,
		maximizable: false,
		autoHideMenuBar: true,
		webPreferences: {
			nodeIntegration: false,
            preload: path.join(__dirname, "./preload/newServerPreload.js")
        },
        icon: path.join(__dirname, "/icon.ico")
	});

	newServerWindow.loadFile('./html/newServer.html')

	newServerWindow.once("ready-to-show", () => {
		newServerWindow.show()
	});

	newServerWindow.on("closed", () => {
		newServerWindow = null
	});
});

ipcMain.on("addServer", (event, name, address, port, client, manifest) => {
    const currentIndex = (serverKeys.length === 0) ? "0" : serverKeys[serverKeys.length - 1];

	userSettings.set("servers." + (parseInt(currentIndex) + 1) + ".name", name);
	userSettings.set("servers." + (parseInt(currentIndex) + 1) + ".address", address);
	userSettings.set("servers." + (parseInt(currentIndex) + 1) + ".port", parseInt(port));
    userSettings.set("servers." + (parseInt(currentIndex) + 1) + ".client", parseInt(client));

    if (manifest.trim() !== "")
        userSettings.set("servers." + (parseInt(currentIndex) + 1) + ".manifest", manifest);

	newServerWindow.close();
	mainWindow.reload();
});

ipcMain.on("launchGame", (event, index) => {
    editLiveConfig(index);
    editLoginConfig(index);
	
    const executableLocation = userSettings.get("clients." + clientKeys[clientKeys.findIndex(item => item === userSettings.get("servers." + index + ".client").toString())] + ".executable");

    childProcess.spawn(executableLocation, [], { cwd: path.dirname(executableLocation), detached: true, stdio: "ignore" }).unref();

    if (userSettings.get("launcher.run_in_background") != true)
        process.exit(0);
    else
        log.info("Launching Star Wars Galaxies...");
});

ipcMain.on("getMultipleInstances", (event, index) => {
    try {
        fs.readFile(path.dirname(userSettings.get("clients." + clientKeys[clientKeys.findIndex(item => item === userSettings.get("servers." + index + ".client").toString())] + ".executable")) + "\\user.cfg", (error, data) => {
            if (error)
                return log.error(error);

            if (data.includes("allowMultipleInstances=")) {
                const begin = data.indexOf("allowMultipleInstances=");
                const subArray = data.subarray(begin, data.length);

                const instancesLine = subArray.toString().split("\n")[0].replace("\r", "");
                const value = instancesLine.toString().split("=")[1];

                mainWindow.webContents.send("multipleInstancesResponse", (value == "false") ? null : value);
            }
        });
    } catch(e) {
        log.error("No client or server section in config.json");
        //log.error(e);
    }
});

ipcMain.on("setMultipleInstances", (event, index, currentValue) => {
    try {
        const cfgPath = path.dirname(userSettings.get("clients." + clientKeys[clientKeys.findIndex(item => item === userSettings.get("servers." + index + ".client").toString())] + ".executable")) + "\\user.cfg";

        fs.readFile(cfgPath, (error, data) => {
            if (error)
                return log.error(error);

            if (data.includes("allowMultipleInstances=")) {
                const instancesRegexp = new RegExp("allowMultipleInstances=(.+)", "i");

                const options = {
                    files: cfgPath,
                    from: instancesRegexp,
                    to: "allowMultipleInstances=" + currentValue
                }
            
                replace(options, (error, results) => { 
                    if (error)
                        return log.info(error);
                });
            } else if (data.includes("[SwgClient]")) {
                const options = {
                    files: cfgPath,
                    from: "[SwgClient]",
                    to: "[SwgClient]\r\n\tallowMultipleInstances=" + currentValue
                }
            
                replace(options, (error, results) => { 
                    if (error)
                        return log.info(error);
                });
            } else {
                fs.appendFile(cfgPath, "\r\n\r\n[SwgClient]\r\n\tallowMultipleInstances=" + currentValue, (error) => {
                    if (error)
                        return log.error(error);
                });
            }
        });
    } catch(e) {
        log.error("No clients or servers found");
        //log.error(e);
    }
});

ipcMain.on("getRunInBackground", () => {
	if (userSettings.has("launcher")) {
		const value = userSettings.get("launcher.run_in_background");

		mainWindow.webContents.send("runInBackgroundResponse", (value == "false") ? null : value);
	}
});

ipcMain.on("setRunInBackground", (event, value) => {
	userSettings.set("launcher.run_in_background", value);
});

ipcMain.on("createManageClientsWindow", () => {
	manageClientsWindow = new BrowserWindow({
		width: 650,
		height: 310,
		title: "Manage Clients",
		parent: mainWindow,
		modal: true,
		show: false,
		minimizable: false,
		maximizable: false,
		autoHideMenuBar: true,
		webPreferences: {
			nodeIntegration: true,
            preload: path.join(__dirname, "./preload/manageClientsPreload.js")
        },
        icon: path.join(__dirname, "/icon.ico")
	});

	manageClientsWindow.loadFile("./html/manageClients.html")

	manageClientsWindow.once("ready-to-show", () => {
		manageClientsWindow.show()
	});

	manageClientsWindow.on("closed", () => {
		manageClientsWindow = null
	});
});

ipcMain.on("browseForExecutable", () => {
	const fileOptions = {
		title: "Find Executable File",
		filters: [
			{ name: "Executable", extensions: ["exe"] }
		],
		defaultPath: "C:\\"
	}

	const filePath = dialog.showOpenDialog(mainWindow, fileOptions);

    try {
        childProcess.exec('wmic datafile where name="'+ filePath[0].split("\\").join("\\\\") + '" get Version', function(error, stdout, stderr) {
            if (!error)
                log.error("Getting executable Version info from WMIC failed in browseForExecutable");

            const executableEra = versionToEraList[stdout.split("\n")[1].trim()];
            let loginPath = path.dirname(filePath[0]);

            if (executableEra === "Pre-CU")
                loginPath += "\\swgemu_login.cfg";
            else
                loginPath += "\\login.cfg";

            manageClientsWindow.webContents.send("browseForExecutableResponse", filePath[0], loginPath, executableEra);
        });
    } catch (e) {
        log.error("No file selected for new executable");
        //log.error(e);
    }
});

ipcMain.on("browseForLoginFile", () => {
	const fileOptions = {
		title: "Find Login File",
		filters: [
			{ name: "Config File", extensions: ["cfg"] }
		],
		defaultPath: "C:\\"
	}

	const filePath = dialog.showOpenDialog(mainWindow, fileOptions);

	try {
		manageClientsWindow.webContents.send("browseForLoginFileResponse", filePath[0]);
	} catch (e) {
		log.error("No file selected for new login file");
		//log.error(e);
	}
});

ipcMain.on("addClient", (event, name, executable, loginFile) => {
    const currentIndex = (clientKeys.length  === 0) ? "0" : clientKeys[clientKeys.length - 1];

	userSettings.set("clients." + (parseInt(currentIndex) + 1) + ".name", name);
	userSettings.set("clients." + (parseInt(currentIndex) + 1) + ".executable", executable);
	userSettings.set("clients." + (parseInt(currentIndex) + 1) + ".loginFile", loginFile);

	manageClientsWindow.reload();
});

ipcMain.on("removeClient", (event, index) => {
    try {
        userSettings.delete("clients." + index);

        manageClientsWindow.reload();
    } catch(e) {
        log.error("No clients found");
        //log.error(e);
    }
});

ipcMain.on("verifyInstallation", (event, serverIndex) => {
	const executablePath = userSettings.get("clients." + clientKeys[clientKeys.findIndex(item => item === userSettings.get("servers." + serverIndex + ".client").toString())] + ".executable");

	childProcess.exec('wmic datafile where name="'+ executablePath.split("\\").join("\\\\") + '" get Version', function(error, stdout, stderr) {
		if (error)
            return log.error("Getting executable Version info from WMIC failed in verifyInstallation");

        let corruptedFiles = [];
        let missingFiles = [];

        const executableVersion = stdout.split("\n")[1].trim();
        const clientEra = versionToEraList[executableVersion].toLowerCase();

        const md5ChecksumsJson = require("./json/md5Checksums.json"); // Checksums use the MD5 algorithm with a Hexidecimal (Base16) encoding
        const milesFiles = Object.keys(md5ChecksumsJson[clientEra].miles);
        const baseFiles = Object.keys(md5ChecksumsJson[clientEra].base);

        mainWindow.webContents.executeJavaScript("document.getElementById('launch').setAttribute('disabled', 'true');");
    
        for (let i = 0; i < milesFiles.length; i++) {
            log.info("Verifying file: " + milesFiles[i]);
    
            if (fs.existsSync(path.dirname(executablePath) + "\\miles\\" + milesFiles[i])) {
                const fileBuffer = fs.readFileSync(path.dirname(executablePath) + "\\miles\\" + milesFiles[i]);
                const generatedHash = crypto.createHash("md5").update(fileBuffer).digest("hex");
                const validHash = md5ChecksumsJson[clientEra].miles[milesFiles[i]];
    
                if (generatedHash.toLowerCase() != validHash.toLowerCase()) {
                    log.error("\tCorrupted");
                    corruptedFiles.push(milesFiles[i]);
                }
            } else {
                log.error("\tMissing");
                missingFiles.push(milesFiles[i]);
            }

            Progress.add(milesFiles.length + baseFiles.length + Object.keys(contentManifest).length)

            mainWindow.webContents.send("updateVerificationProgress", Progress.get());
        }
    
        for (let i = 0; i < baseFiles.length; i++) {
            log.info("Verifying file: " + baseFiles[i]);
    
            if (fs.existsSync(path.dirname(executablePath) + "\\" + baseFiles[i])) {
                const fileBuffer = fs.readFileSync(path.dirname(executablePath) + "\\" + baseFiles[i]);
                const generatedHash = crypto.createHash("md5").update(fileBuffer).digest("hex");
                const validHash = md5ChecksumsJson[clientEra].base[baseFiles[i]];
    
                if (generatedHash.toLowerCase() != validHash.toLowerCase()) {
                    log.error("\tCorrupted");
                    corruptedFiles.push(baseFiles[i]);
                }
            } else {
                log.error("\tMissing");
                missingFiles.push(baseFiles[i]);
            }

            Progress.add(milesFiles.length + baseFiles.length + Object.keys(contentManifest).length)

            mainWindow.webContents.send("updateVerificationProgress", Progress.get());
        }
        
        for (let i = 0; i < Object.keys(contentManifest).length; i++) {
            log.info("Verifying extra server files: " + contentManifest[i].name);

            if (fs.existsSync(path.dirname(executablePath) + "\\" + contentManifest[i].name)) {
                const fileBuffer = fs.readFileSync(path.dirname(executablePath) + "\\" + contentManifest[i].name);
                const generatedHash = crypto.createHash("md5").update(fileBuffer).digest("hex");
                const validHash = contentManifest[i].md5;

                if (generatedHash.toLowerCase() != validHash.toLowerCase()) {
                    log.error("\tCorrupted");
                    corruptedFiles.push(contentManifest[i].name);
                }
            } else {
                log.error("\tMissing");
                missingFiles.push(contentManifest[i].name);
            }
            
            Progress.add(milesFiles.length + baseFiles.length + Object.keys(contentManifest).length)

            mainWindow.webContents.send("updateVerificationProgress", Progress.get());
        }

        mainWindow.webContents.executeJavaScript("document.getElementById('launch').removeAttribute('disabled');");
        mainWindow.webContents.executeJavaScript(`document.getElementById("verifyProgressBackground").hidden = true;`);
        
        Progress.reset();

        showVerificationResponseDialog(corruptedFiles, missingFiles);
	});
});

ipcMain.on("checkForClients", (event) => {
    serverKeys = (!userSettings.has("servers")) ? [] : Object.keys(userSettings.get("servers"));
    clientKeys = (!userSettings.has("clients")) ? [] : Object.keys(userSettings.get("clients"));

	if (!userSettings.has("servers") || serverKeys.length == 0) {
        mainWindow.webContents.executeJavaScript("document.getElementById('verify').setAttribute('disabled', 'true');");
        mainWindow.webContents.executeJavaScript("document.getElementById('launch').setAttribute('disabled', 'true');");
    }

    if (!userSettings.has("clients") || clientKeys.length == 0) {
        mainWindow.webContents.executeJavaScript("document.getElementById('multipleInstances').setAttribute('disabled', 'true');");
    }
});

ipcMain.on("checkForExtraContent", (event) => {
    mainWindow.webContents.executeJavaScript("document.getElementById('serverList')[document.getElementById('serverList').selectedIndex].value", false).then((serverId) => {
        contentManifest = {}; // Empty out the manifest every time we call this so that we can start fresh

        if (!userSettings.has("servers." + serverId + ".manifest")) {
            log.error("The manifest URL does not exist for server: " + userSettings.get("servers." + serverId + ".name"));

            mainWindow.webContents.send("checkForExtraContentResponse");

            return;
        }

        const contentUrl = userSettings.get("servers." + serverId + ".manifest").toString();

        try {
            https.get(contentUrl, (response) => {
                if (response.statusCode !== 200) {
                    log.error("Response status was " + response.statusCode + " for " + contentUrl);

                    mainWindow.webContents.send("serverStatusResponse", "unknown");

                    return;
                }

                mainWindow.webContents.send("checkForExtraContentResponse");

                let body = "";

                response.on("data", (chunk) => {
                    body += chunk;
                });

                response.on("end", () => {
                    contentManifest = JSON.parse(body);

                    for (let i = 0; i < contentManifest["required"].length; i++) {
                        // This launcher is server agnostic but allows all servers to be added and connected to; remove any loose files (mods) from the manifest and only keep the tre files for simplicity
                        if (!contentManifest["required"][i].name.includes(".tre")) {
                            delete contentManifest["required"][i];
                            continue;
                        }

                        // Reorganize the object to not have to go down to "required" everytime
                        contentManifest[i] = {
                            name: contentManifest["required"][i].name,
                            size: contentManifest["required"][i].size,
                            md5: contentManifest["required"][i].md5,
                            url: contentManifest["required"][i].url
                        }

                        delete contentManifest["required"][i];
                    }

                    delete contentManifest["required"];

                    log.info("Retrieved manifest for server: " + userSettings.get("servers." + serverId + ".name").toString());

                    // Check if extra content is downloaded, notify user that extra content is available for the server if it is not
                    const executablePath = userSettings.get("clients." + userSettings.get("servers." + serverId + ".client") + ".executable");
                    
                    let totalDownloadSize = 0;
                    let contentToDownload = [];

                    for (let i = 0; i < Object.keys(contentManifest).length; i++) {
                        if (!fs.existsSync(path.dirname(executablePath) + "\\" + contentManifest[i].name)) {
                            contentToDownload.push(contentManifest[i].name);
                            totalDownloadSize += contentManifest[i].size;
                        }
                    }

                    if (contentToDownload.length > 0) {
                        dialog.showMessageBox(mainWindow, {
                            type: "question",
                            title: userSettings.get("servers." + serverId + ".name"),
                            message: "Extra content for this server is available. Download now?",
                            detail: "This server has extra content that is required to download before connecting. If you dismiss this notice, connecting to the server will most likely fail.\n\nThis message will always appear when new content that is not already downloaded is detected.",
                            buttons: ["Dismiss", "Download"],
                            noLink: true
                        }, (response) => {
                            if (response == 1) {
                                mainWindow.webContents.executeJavaScript("document.getElementById('launch').setAttribute('disabled', 'true');");

                                for (let i = 0; i < contentToDownload.length; i++) {
                                    for (let j = 0; j < Object.keys(contentManifest).length; j++) {
                                        if (contentManifest[j].name == contentToDownload[i])
                                            downloadFile(contentManifest[j].url, path.dirname(executablePath) + "\\" + contentManifest[j].name, totalDownloadSize, true);
                                    }
                                }
                            }
                        });
                    }

                    // Replace live.cfg with default version so that when the Launch Button is pressed the live.cfg can be editted automatically with the required extra content
                    const stdout = childProcess.execSync('wmic datafile where name="'+ executablePath.split("\\").join("\\\\") + '" get Version');
                    const executableVersion = stdout.toString().split("\n")[1].trim();
                    const clientEra = versionToEraList[executableVersion].toLowerCase();
            
                    let cfgPath = path.dirname(executablePath);
            
                    if (clientEra == "pre-cu")
                        cfgPath += "\\swgemu_live.cfg";
                    else
                        cfgPath += "\\live.cfg"

                    const md5ChecksumsJson = require("./json/md5Checksums.json");

                    const fileBuffer = fs.existsSync(cfgPath) ? fs.readFileSync(cfgPath) : Buffer.alloc(0);

                    const generatedHash = crypto.createHash("md5").update(fileBuffer).digest("hex");
                    const validHash = md5ChecksumsJson[clientEra].base["live.cfg"];

                    if (generatedHash.toLowerCase() != validHash.toLowerCase()) {
                        try {
                            fs.unlinkSync(cfgPath); // Delete the CFG file before actually downloading it, otherwise the downloadFile() function appends (for the custom content)
                        } catch(e) {
                            log.error("Deletion attempt failed for " + cfgPath);
                        }

                        if (clientEra == "pre-cu")
                            downloadFile("https://onedrive.live.com/download?cid=362EE92E86BF67EC&resid=362EE92E86BF67EC%21460&authkey=ACIdZerGRhyaJmk", cfgPath, 2115, false);
                        else if (clientEra == "cu")
                            downloadFile("https://onedrive.live.com/download?cid=362EE92E86BF67EC&resid=362EE92E86BF67EC%21457&authkey=AMqXlPBjnwtop_g", cfgPath, 509, false);
                        else if (clientEra == "nge")
                            downloadFile("https://onedrive.live.com/download?cid=362EE92E86BF67EC&resid=362EE92E86BF67EC%21461&authkey=ADc9_9bNa6VB050", cfgPath, 488, false);
                    }
                });
            });
        } catch(e) {
            log.error("The manifest URL may be malformed as it cannot be reached for server: " + userSettings.get("servers." + index + ".name"));
        }
    })
});

ipcMain.on("disableLaunchButton", (event) => {
    mainWindow.webContents.executeJavaScript("document.getElementById('launch').setAttribute('disabled', 'true');");
});

ipcMain.on("log", (event, type, string) => {
    if (type == "info")
        log.info(string);
    else if (type == "error")
        log.error(string);
});

// TODO: FPS Setter for SWGEmu and CU (not really sure what we can do with the NGE here?)
// TODO: Screenshot directory change for SWGEmu and CU (once again, unsure about the NGE)
// TODO: Add slow movement mod for SWGEmu and potentially CU as well (not sure if it works for the NGE, or if it's even desired for that era)

// Keep these at the bottom so that we can keep it updated if we do any changes above
global.userSettings = userSettings;
global.packageInfo = packageInfo;