const { ipcRenderer, remote } = require("electron");
const userSettings = remote.getGlobal("userSettings");

function generateClientList() {
    const clientList = userSettings.get("clients");

    for (i = 0; i < Object.keys(clientList).length; i++) {
        const keys = Object.keys(clientList);

        let option = document.createElement("option");
        option.text = clientList[keys[i]].name;
        option.value = keys[i];

        document.getElementById("clientList").appendChild(option);
        //ipcRenderer.send("log", "info", clientList[keys[i]].name);
    }
}

function getClientIndex() {
    let listIdx = document.getElementById("clientList").selectedIndex;

    return document.getElementById("clientList")[listIdx].value;
}

document.addEventListener("DOMContentLoaded", function(data) {
    generateClientList();
    
    document.getElementById("addClientForm").onsubmit = function() {
        let formElement = document.forms.addClientForm;
        let clientInfo = new FormData(formElement);

        // TODO: Validation?

        ipcRenderer.send("addClient", clientInfo.get("name"), clientInfo.get("executable"), clientInfo.get("loginFile"));
    }

    document.getElementById("executableBrowse").onclick = function() {
        ipcRenderer.send("browseForExecutable");
    }

    ipcRenderer.on("browseForExecutableResponse", (event, filePath, loginPath, executableVersion) => {
        document.getElementById("executable").value = filePath;
        document.getElementById("loginFile").value = loginPath;

        if (!document.getElementById("name").value)
            document.getElementById("name").value = executableVersion;
    });

    document.getElementById("loginFileBrowse").onclick = function() {
        ipcRenderer.send("browseForLoginFile");
    }

    ipcRenderer.on("browseForLoginFileResponse", (event, value) => {
        document.getElementById("loginFile").value = value;
    });

    document.getElementById("removeClient").onclick = function() {
        ipcRenderer.send("removeClient", getClientIndex());
    }
});