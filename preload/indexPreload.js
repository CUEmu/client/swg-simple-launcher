const { ipcRenderer, remote } = require("electron");
const userSettings = remote.getGlobal("userSettings");

function generateClientList() {
    const clientList = userSettings.get("clients");

    for (let i = 0; i < Object.keys(clientList).length; i++) {
        const clientKeys = Object.keys(clientList);

        let option = document.createElement("option");
        option.text = clientList[clientKeys[i]].name;
        option.value = clientKeys[i];

        document.getElementById("clientList").appendChild(option);
    }
}

function generateServerList(idx) {
    document.getElementById("serverList").options.length = 0; // Empty the element before appending new ones
    
    const serverList = userSettings.get("servers");

    for (let i = 0; i < Object.keys(serverList).length; i++) {
        const serverKeys = Object.keys(serverList);

        if (serverList[serverKeys[i]].client != idx)
            continue;

        let option = document.createElement("option");
        option.text = serverList[serverKeys[i]].name;
        option.value = serverKeys[i];

        document.getElementById("serverList").appendChild(option);
    }
}

function getClientIndex() {
    let listIdx = document.getElementById("clientList").selectedIndex;

    return document.getElementById("clientList")[listIdx].value;
}

function getServerIndex() {
    let listIdx = document.getElementById("serverList").selectedIndex;

    return document.getElementById("serverList")[listIdx].value;
}

document.addEventListener("DOMContentLoaded", function(data) {
    ipcRenderer.send("checkForClients");

    // Generate the client and server lists on load to kick things off
    generateClientList();
    generateServerList(1);

    ipcRenderer.send("getRunInBackground");
    ipcRenderer.send("getMultipleInstances", getServerIndex());

    ipcRenderer.send("checkForExtraContent");

    document.getElementById("clientList").onchange = function() {
        generateServerList(getClientIndex());

        // Force serverList element to update the "onchange" event to trigger server status updating, etc. for the first server index when changing client
        document.getElementById("serverList").dispatchEvent(new Event("change", { bubbles: true} ));
    }

    document.getElementById("serverList").onchange = function() {
        document.getElementById("serverStatus").innerHTML = "...";

        ipcRenderer.send("disableLaunchButton");
        ipcRenderer.send("getMultipleInstances", getServerIndex());

        ipcRenderer.send("checkForExtraContent");
    }
    
    document.getElementById("removeServer").onclick = function() {
        ipcRenderer.send("removeServer", getServerIndex());
    }

    document.getElementById("addServer").onclick = function() {
        ipcRenderer.send("createNewServerWindow");
    }

    document.getElementById("launch").onclick = function() {
        ipcRenderer.send("launchGame", getServerIndex());
    }

    document.getElementById("multipleInstances").onchange = function() {
        ipcRenderer.send("setMultipleInstances", getServerIndex(), this.checked);
    }

    document.getElementById("runInBackground").onchange = function() {
        ipcRenderer.send("setRunInBackground", this.checked);
    }

    document.getElementById("manageClients").onclick = function() {
        ipcRenderer.send("createManageClientsWindow");
    }

    document.getElementById("verify").onclick = function() {
        ipcRenderer.send("verifyInstallation", getServerIndex());
    }
});

ipcRenderer.on("checkForExtraContentResponse", function(event, value) {
    ipcRenderer.send("getServerStatus", getServerIndex());
});

ipcRenderer.on("serverStatusResponse", function(event, message) {
    let status = "";

    if (message == "up")
        status = "Online";
    else if (message == "locked")
        status = "Locked";
    else if (message == "loading")
        status = "Loading...";
    else if (message == "unknown")
        status = "Offline";

    document.getElementById("serverStatus").innerHTML = status;
});

ipcRenderer.on("multipleInstancesResponse", function(event, value) {
    document.getElementById("multipleInstances").checked = value;
});

ipcRenderer.on("runInBackgroundResponse", function(event, value) {
    document.getElementById("runInBackground").checked = value;
});

ipcRenderer.on("updateVerificationProgress", function(event, percentage) {
    const progressBackground = document.getElementById("verifyProgressBackground");

    if (progressBackground.hasAttribute("hidden"))
        progressBackground.removeAttribute("hidden");

    const progressBarForeground = document.getElementById("verifyProgressForeground");
    
    progressBarForeground.style.width = percentage + "%";
});