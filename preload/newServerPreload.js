const { ipcRenderer, remote } = require("electron");
const userSettings = remote.getGlobal("userSettings");

function generateClientList() {
    const clientList = userSettings.get("clients");

    for (i = 0; i < Object.keys(clientList).length; i++) {
        const keys = Object.keys(clientList);

        let option = document.createElement("option");
        option.text = clientList[keys[i]].name;
        option.value = keys[i];

        document.getElementById("clientList").appendChild(option);
        //ipcRenderer.send("log", "info", clientList[keys[i]].name);
    }
}

document.addEventListener("DOMContentLoaded", function(data) {
    generateClientList();

    document.getElementById("addServerForm").onsubmit = function(e) {
        let formElement = document.forms.addServerForm;
        let serverInfo = new FormData(formElement);
        
        // TODO: Validation?

        ipcRenderer.send("addServer", serverInfo.get("name"), serverInfo.get("address"), serverInfo.get("port"), serverInfo.get("client"), serverInfo.get("content"));
    }
});